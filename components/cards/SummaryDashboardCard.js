import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';

const useStyles = makeStyles({
  card: {
    width: '100%'
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

export default function SimpleCard({ cardTitle, totalNumber, previousNumber, link }) {
  const classes = useStyles();
  const Difference = () => {
    let result = totalNumber - previousNumber
    let percentResult = Math.floor(((totalNumber - previousNumber) / previousNumber) * 100)
    let percentColor
    if (Math.sign(result) === 1) {
      percentColor = "red"
    } else {
      percentColor = "#00c437"
    }
    return (
      <Typography align="center" variant="body2" component="p">
        {Math.sign(result) === 1 ? "+" + result : result}
        <span style={{ color: percentColor }}> ({Math.sign(percentResult) === 1 ? percentResult + "%" : Math.abs(percentResult) + "%"})</span>
      </Typography>
    )
  };

  return (
    <Card className={classes.card}>
      <Link underline="none" color="inherit" href={link}>
        <CardContent>
          <Typography align="center" variant="h5" component="h2">
            {cardTitle}
          </Typography>
          <Typography align="center" variant="h2" color="textSecondary">
            {totalNumber}
          </Typography>
          {cardTitle == "Visits Not Checked In" ? <Typography align="center" variant="body2" component="p">Today</Typography> : <Difference />}
        </CardContent>
      </Link>
    </Card>
  );
}
