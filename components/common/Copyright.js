import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import styled from 'styled-components';

const Copyright = () =>{
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://vida.co.uk/">
        Vida.
      </Link>{' '}
      <br />
      All Rights Reserved. {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

export default Copyright;
