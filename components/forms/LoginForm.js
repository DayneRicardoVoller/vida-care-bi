import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import useForm from "react-hook-form";
import { useDispatch } from "react-redux";

import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { setSession } from "../../store/ducks/session";

const useStyles = makeStyles((theme) => ({
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const mutation = gql`
  mutation($email: String!, $password: String!) {
    createUserSession(email: $email, password: $password) {
      id
      user {
        email
        id
      }
    }
  }
`;

const LoginForm = ({ onChangeToSignUp: pushChangeToSignUp }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const {
    formState: { isSubmitting },
    handleSubmit,
    register,
  } = useForm();
  const [createUserSession] = useMutation(mutation);

  const onSubmit = handleSubmit(async ({ email, password }) => {
    const {
      data: { createUserSession: createdSession },
    } = await createUserSession({
      variables: {
        email,
        password,
      },
    });
    dispatch(setSession(createdSession));
  });

  return (
    <div className={classes.paper}>
      <img src="Vida_Logo.png" style={{ marginBottom: "20px" }} />
      <Typography component="h1" variant="h5">
        Login
      </Typography>
      <form className={classes.form} noValidate onSubmit={onSubmit}>
        <input
          style={{
            width: "46%",
            marginRight: "1rem",
            height: "2rem",
          }}
          disabled={isSubmitting}
          name="email"
          type="email"
          placeholder="Email"
          autoComplete="on"
          ref={register}
        />
        <input
          style={{ width: "46%", height: "2rem" }}
          disabled={isSubmitting}
          name="password"
          type="password"
          placeholder="Password"
          autoComplete="on"
          ref={register}
        />
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          className={classes.submit}
          disabled={isSubmitting}
        >
          <Link
            href="/dashboard"
            variant="body2"
            style={{ color: "#fff", textDecoration: "none" }}
          >
          Login
          </Link>
        </Button>
        <Grid container>
          <Grid item xs>
            <Link href="/reset-password" variant="body2">
              Forgot password?
            </Link>
          </Grid>
          <Grid item>
            <Link href="/signup" variant="body2">
              Don't have an account? Sign Up
            </Link>
          </Grid>
        </Grid>
      </form>
    </div>
  );
};

export default LoginForm;
