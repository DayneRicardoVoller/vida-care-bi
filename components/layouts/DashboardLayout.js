import clsx from 'clsx';
import Link from 'next/link';
import Copyright from '../common/Copyright';
import { makeStyles } from '@material-ui/core/styles';
import { CssBaseline, Drawer, AppBar, Toolbar, Typography, Divider, IconButton, Badge, List, ListItem, ListItemIcon, ListItemText, Box } from '@material-ui/core'
import ViewModuleIcon from '@material-ui/icons/ViewModule';
import TimelineIcon from '@material-ui/icons/Timeline';
import AnnouncementIcon from '@material-ui/icons/Announcement';
import PollIcon from '@material-ui/icons/Poll';
import TableChartIcon from '@material-ui/icons/TableChart';
import MenuIcon from '@material-ui/icons/Menu';
import NoteIcon from '@material-ui/icons/Note';
import NotificationsIcon from '@material-ui/icons/Notifications';

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
  inline: {
    color: '#f50057 !important'
  }
}));

const DashboardLayout = (props) => {
  const classes = useStyles();

  const [open, setOpen] = React.useState(true);
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };
  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="absolute" className={clsx(classes.appBar, open && classes.appBarShift)}>
        <Toolbar className={classes.toolbar}>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
          >
            <MenuIcon />
          </IconButton>
          <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
            Dashboard
          </Typography>
          <IconButton color="inherit">
            <Badge badgeContent={null} color="secondary">
              <NotificationsIcon />
            </Badge>
          </IconButton>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        classes={{
          paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
        }}
        open={open}
      >
        <div style={{ display: "flex", justifyContent: "center" }}>
          <img src='../Vida_Logo.png' style={{ margin: "20px 0 0 0", width: "28%" }} />
        </div>
        <List component="nav" aria-label="main mailbox folders">
          <Divider />
          <Link href="/dashboard">
            <ListItem button>
              <ListItemIcon>
                <ViewModuleIcon />
              </ListItemIcon>
              <ListItemText className={classes.inline} primary="All" />
            </ListItem>
          </Link>
          <Divider />
          <Link href="/dashboard/unassigned-calls-chart">
            <ListItem button>
              <ListItemIcon>
                <TimelineIcon />
              </ListItemIcon>
              <ListItemText className={classes.inline} primary="Unassigned Calls" />
            </ListItem>
          </Link>
          <Divider />
          <Link href="/dashboard/visits-not-checked-in">
            <ListItem button>
              <ListItemIcon>
                <TableChartIcon />
              </ListItemIcon>
              <ListItemText className={classes.inline} primary="Not Checked In" />
            </ListItem>
          </Link>
          <Divider />
          <Link href="/dashboard/late-calls">
            <ListItem button>
              <ListItemIcon>
                <PollIcon />
              </ListItemIcon>
              <ListItemText className={classes.inline} primary="Late Calls" />
            </ListItem>
          </Link>
          <Divider />
          <Link href="/dashboard/missed-calls">
            <ListItem button>
              <ListItemIcon>
                <AnnouncementIcon />
              </ListItemIcon>
              <ListItemText className={classes.inline} primary="Missed Calls" secondary={
                <React.Fragment>
                  <Typography
                    component="span"
                    className={classes.inline}
                    color="textPrimary"
                  >
                    Incidents Reported
                  </Typography>
                </React.Fragment>
              } />
            </ListItem>
          </Link>
          <Divider />
          <Link href="/dashboard/missing-notes">
            <ListItem button>
              <ListItemIcon>
                <NoteIcon />
              </ListItemIcon>
              <ListItemText className={classes.inline} primary="Missing Notes" />
            </ListItem>
          </Link>
          <Divider />
        </List>
        <Box style={{ position: "absolute", bottom: "20px", left: "15%" }} mt={5}>
          <Copyright />
        </Box>
      </Drawer>
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        {props.children}
      </main>
    </div>
  );
}

export default DashboardLayout;
