import gql from "graphql-tag";
import React from "react";
import { useQuery } from "react-apollo";
import styled from "styled-components";

import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Box from "@material-ui/core/Box";
import DashboardLayout from "../../components/layouts/DashboardLayout";
import SummaryDashboardCard from "../../components/cards/SummaryDashboardCard";

const useStyles = makeStyles((theme) => ({
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 240,
  },
}));

const query = gql`
  {
    appointments(start: "2020-01-01T00:00:00Z", end: "2020-01-28T00:00:00Z") {
      identifier
      client
      carer
      startDate
      endDate
      notesPublic
      notesPrivate
      cancelled
      cancellationReason
    }
  }
`;


function DashboardIndexPage() {
  const classes = useStyles();
  const { data, loading, refetch } = useQuery(query);

  if (loading) return "Loading...";
  // Counts
  const unassignedCalls = 0;
  const notCheckedIn = 0;
  const lateCalls = 0;
  const incidents = 0;
  const missedCalls = 0;
  const missingNotes = 0;

  return (
    <DashboardLayout>
      <Container maxWidth="md" className={classes.container}>
        {/* {data.appointments.map((appointment) => (
          <div>{appointment.identifier}</div>
        ))} */}
        <div
          style={{
            width: "100%",
            display: "flex",
            alignItems: "flex-end",
            justifyContent: "center",
            marginBottom: "30px",
          }}
        >
          <Box component="div" display="inline">
            <form className={classes.form} noValidate>
              <TextField
                style={{ fontSize: "0.875rem", fontWeight: "500" }}
                id="fromDate"
                label="from"
                type="date"
                defaultValue="2020-05-21"  //{appointment.dates.start} 
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
              />
              <TextField
                style={{ fontSize: "0.875rem", fontWeight: "500" }}
                id="toDate"
                label="to"
                type="date"
                defaultValue="2020-05-28" //{appointment.dates.end} 
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </form>
          </Box>
        </div>
        <Grid container spacing={3}>
          <Grid item xs={12} md={4}>
            <SummaryDashboardCard
              cardTitle="Unassigned Calls"
              totalNumber="92" 
              previousNumber="130"
              link="/dashboard/unassigned-calls-chart"
            />
          </Grid>
          <Grid item xs={12} md={4}>
            <SummaryDashboardCard
              cardTitle="Visits Not Checked In"
              totalNumber="23"
              previousNumber="7"
              link="/dashboard/visits-not-checked-in"
            />
          </Grid>
          <Grid item xs={12} md={4}>
            <SummaryDashboardCard
              cardTitle="Late Calls"
              totalNumber="13"
              previousNumber="8"
              link="/dashboard/late-calls"
            />
          </Grid>
          <Grid item xs={12} md={4}>
            <SummaryDashboardCard
              cardTitle="Incidents Reported"
              totalNumber="11"
              previousNumber="4"
              link="/dashboard/missed-calls"
            />
          </Grid>
          <Grid item xs={12} md={4}>
            <SummaryDashboardCard
              cardTitle="Missed Calls"
              totalNumber="16"
              previousNumber="15"
              link="/dashboard/missed-calls"
            />
          </Grid>
          <Grid item xs={12} md={4}>
            <SummaryDashboardCard
              cardTitle="Missing Notes"
              totalNumber="2"
              previousNumber="3"
              link="/dashboard/missing-notes"
            />
          </Grid>
        </Grid>
      </Container>
    </DashboardLayout>
  );
}

export default DashboardIndexPage;
