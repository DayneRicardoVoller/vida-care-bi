import React, { PureComponent } from "react";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ReferenceLine,
  ResponsiveContainer,
} from "recharts";
import DashboardLayout from "../../components/layouts/DashboardLayout";
import {
  TextField,
  Container,
  Button,
  Typography,
  FormControlLabel,
  Checkbox,
  Box,
  Paper,
  FormGroup,
  FormLabel,
  FormControl,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const data = [
  {
    name: "21 May",
    "% of Late Calls": 2,
    pv: 2,
  },
  {
    name: "22 May",
    "% of Late Calls": 1,
    pv: 7,
  },
  {
    name: "23 May",
    "% of Late Calls": 2,
    pv: 3,
  },
  {
    name: "24 May",
    "% of Late Calls": 3,
    pv: 5,
  },
  {
    name: "25 May",
    "% of Late Calls": 4,
    pv: 8,
  },
  {
    name: "26 May",
    "% of Late Calls": 1,
    pv: 3,
  },
  {
    name: "27 May",
    "% of Late Calls": 0,
    pv: 1,
  },
];

const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 650,
  },
  title: {
    margin: "30px",
  },
  container: {
    display: "inline",
    flexWrap: "wrap",
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
    fontSize: "0.875rem",
    fontWeight: "500",
  },
  root: {
    display: "flex",
    flexWrap: "wrap",
    "& > *": {
      width: "250px",
    },
  },
}));

export default function MissedCalls() {
  const classes = useStyles();
  const [state, setState] = React.useState({
    liveInCare: true,
    dailyCare: false,
    nightCare: false,
    emergencyRatingLow: true,
    emergencyRatingMedium: false,
    emergencyRatingHigh: false,
    greenwich: true,
    kingston: false,
  });

  const handleChange = (name) => (event) => {
    setState({ ...state, [name]: event.target.checked });
  };

  const {
    liveInCare,
    dailyCare,
    nightCare,
    emergencyRatingLow,
    emergencyRatingMedium,
    emergencyRatingHigh,
    greenwich,
    kingston,
  } = state;

  const resetFilters = () => {
    console.log("reset");
    setState({
      liveInCare: true,
      dailyCare: false,
      nightCare: false,
      emergencyRatingLow: true,
      emergencyRatingMedium: false,
      emergencyRatingHigh: false,
      kingston: false,
    });
  };

  return (
    <DashboardLayout>
      <div style={{ display: "flex", height: "calc(100% - 64px)" }}>
        <ResponsiveContainer width="99%">
          <Container maxWidth="md" className={classes.container}>
            <Typography
              className={classes.title}
              align="center"
              component="h1"
              variant="h6"
              color="inherit"
              noWrap
            >
              Late Calls
            </Typography>
            <LineChart
              width={730}
              height={250}
              data={data}
              style={{ margin: "0 auto" }}
              margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
            >
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="name" />
              <YAxis />
              <Tooltip />
              <Legend />
              <Line dataKey="% of Late Calls" stroke="#82ca9d" />
            </LineChart>
            <div
              style={{
                display: "flex",
                marginTop: "20px",
                marginLeft: "150px",
              }}
            >
              <Box component="div" display="inline" style={{ width: "200px" }}>
                <Typography
                  style={{
                    padding: "8px",
                    fontSize: "0.875rem",
                    fontWeight: "500",
                    border: "2px solid black",
                    display: "inline",
                  }}
                >
                  TOTAL: 13
                </Typography>
              </Box>
            </div>
          </Container>
        </ResponsiveContainer>
        <div className={classes.root}>
          <Paper style={{ paddingTop: "30px" }}>
            <form className={classes.form} noValidate>
              <TextField
                id="fromDate"
                label="from"
                type="date"
                defaultValue="2020-05-21"
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
              />
              <TextField
                id="toDate"
                label="to"
                type="date"
                defaultValue="2020-05-27"
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </form>
            <Container style={{ margin: "30px 0 10px 0" }}>
              <FormControl
                component="fieldset"
                className={classes.formControl}
                style={{ width: "100%" }}
              >
                <FormLabel component="legend">Care Type</FormLabel>
                <FormGroup>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={liveInCare}
                        onChange={handleChange("liveInCare")}
                        value="liveInCare"
                      />
                    }
                    label="Live-in Care"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={dailyCare}
                        onChange={handleChange("dailyCare")}
                        value="dailyCare"
                      />
                    }
                    label="Daily Care"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={nightCare}
                        onChange={handleChange("nightCare")}
                        value="nightCare"
                      />
                    }
                    label="Night Care"
                  />
                </FormGroup>
              </FormControl>
            </Container>

            <Container style={{ margin: "30px 0 10px 0" }}>
              <FormControl
                component="fieldset"
                className={classes.formControl}
                style={{ width: "100%" }}
              >
                <FormLabel component="legend">Emergency rating</FormLabel>
                <FormGroup>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={emergencyRatingLow}
                        onChange={handleChange("emergencyRatingLow")}
                        value="emergencyRatingLow"
                      />
                    }
                    label="Low"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={emergencyRatingMedium}
                        onChange={handleChange("emergencyRatingMedium")}
                        value="emergencyRatingMedium"
                      />
                    }
                    label="Medium"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={emergencyRatingHigh}
                        onChange={handleChange("emergencyRatingHigh")}
                        value="emergencyRatingHigh"
                      />
                    }
                    label="High"
                  />
                </FormGroup>
              </FormControl>
            </Container>

            <Container style={{ marginTop: "30px" }}>
              <FormControl
                component="fieldset"
                className={classes.formControl}
                style={{ width: "100%" }}
              >
                <FormLabel component="legend">Region</FormLabel>
                <FormGroup>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={greenwich}
                        onChange={handleChange("greenwich")}
                        value="greenwich"
                      />
                    }
                    label="Greenwich"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={kingston}
                        onChange={handleChange("kingston")}
                        value="kingston"
                      />
                    }
                    label="Kingston"
                  />
                </FormGroup>
              </FormControl>
            </Container>

            <div style={{ display: "flex", justifyContent: "space-around" }}>
              <Button
                aria-controls="customized-menu"
                aria-haspopup="true"
                variant="contained"
                color="primary"
                style={{ fontSize: "12px", margin: "0 5px 0 5px" }}
              >
                Apply Now
              </Button>
              <Button
                aria-controls="customized-menu"
                aria-haspopup="true"
                variant="contained"
                onClick={resetFilters}
                style={{ fontSize: "12px", margin: "0 5px 0 5px" }}
              >
                Reset
              </Button>
            </div>
          </Paper>
        </div>
      </div>
    </DashboardLayout>
  );
}
