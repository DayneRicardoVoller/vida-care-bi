import React from 'react';
import {
  XAxis, YAxis, CartesianGrid, Tooltip, Legend, BarChart, Bar,
} from 'recharts';
import DashboardLayout from '../../components/layouts/DashboardLayout';
import { TextField, Container, Button, Paper, Typography, FormGroup, FormLabel, FormControlLabel, FormControl, Checkbox, Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const data = [
  {
    name: '21 May', "Number of Missed Calls": 2, "Number of Incidents": 1,
  },
  {
    name: '22 May', "Number of Missed Calls": 1, "Number of Incidents": 3,
  },
  {
    name: '23 May', "Number of Missed Calls": 2, "Number of Incidents": 1,
  },
  {
    name: '24 May', "Number of Missed Calls": 3, "Number of Incidents": 2,
  },
  {
    name: '25 May', "Number of Missed Calls": 6, "Number of Incidents": 1,
  },
  {
    name: '26 May', "Number of Missed Calls": 2, "Number of Incidents": 2,
  },
  {
    name: '27 May', "Number of Missed Calls": 6, "Number of Incidents": 1,
  },
];


const useStyles = makeStyles(theme => ({
  table: {
    minWidth: 650,
  },
  title: {
    margin: "30px"
  },
  container: {
    display: 'inline',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
    fontSize: "0.875rem",
    fontWeight: "500"
  }, root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      width: '250px',
    },
    formControl: {
      margin: theme.spacing(3),
      width: '100%'
    },
  },
}));


export default function MissedCalls() {

  const classes = useStyles();
  const [state, setState] = React.useState({
    liveInCare: true,
    dailyCare: false,
    nightCare: false,
    emergencyRatingLow: true,
    emergencyRatingMedium: false,
    emergencyRatingHigh: false,
    greenwich: true,
    kingston: false
  });

  const handleChange = name => event => {
    setState({ ...state, [name]: event.target.checked });
  };

  const { liveInCare, dailyCare, nightCare, emergencyRatingLow, emergencyRatingMedium, emergencyRatingHigh, greenwich, kingston } = state;

  const resetFilters = () => {
    console.log("reset")
    setState({
      liveInCare: true,
      dailyCare: false,
      nightCare: false,
      emergencyRatingLow: true,
      emergencyRatingMedium: false,
      emergencyRatingHigh: false,
      greenwich: true,
      kingston: false
    });
  }

  return (
    <DashboardLayout >
      <div style={{ display: "flex", height: 'calc(100% - 64px)' }}>

        <Container maxWidth='md' className={classes.container}>
          <Typography className={classes.title} align="center" component="h1" variant="h6" color="inherit" noWrap>Missed Calls and Incidents Reported</Typography>
          <BarChart
            width={730} height={250} data={data} style={{ margin: "0 auto" }}
            margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Bar dataKey="Number of Missed Calls" fill="#82ca9d" />
            <Bar dataKey="Number of Incidents" fill="#8fa1fc" />
          </BarChart>
          <div style={{
            display: "flex", justifyContent: "center", marginTop: "20px"
          }}>
            <Box component="div" display="inline" style={{ width: "200px" }} >
              <Typography style={{ padding: "8px", fontSize: "0.875rem", fontWeight: "500", border: "2px solid black", display: "inline" }} >TOTAL MISSED CALLS: 16</Typography>
            </Box>
            <Box component="div" display="inline" style={{ width: "200px" }}  >
              <Typography style={{ padding: "8px", fontSize: "0.875rem", fontWeight: "500", border: "2px solid black", display: "inline" }} >TOTAL INCIDENTS: 11</Typography> 
            </Box>
          </div>
        </Container>
        <div className={classes.root}>
          <Paper style={{ paddingTop: '30px' }}>
            <form className={classes.form} noValidate>
              <TextField
                id="fromDate"
                label="from"
                type="date"
                defaultValue="2020-05-21"
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
              />
              <TextField
                id="toDate"
                label="to"
                type="date"
                defaultValue="2020-05-27"
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </form>
            <Container style={{ margin: "30px 0 10px 0" }}>
              <FormControl component="fieldset" className={classes.formControl} style={{ width: "100%" }}>
                <FormLabel component="legend">Care Type</FormLabel>
                <FormGroup>
                  <FormControlLabel
                    control={<Checkbox checked={liveInCare} onChange={handleChange('liveInCare')} value="liveInCare" />}
                    label="Live-in Care"
                  />
                  <FormControlLabel
                    control={<Checkbox checked={dailyCare} onChange={handleChange('dailyCare')} value="dailyCare" />}
                    label="Daily Care"
                  />
                  <FormControlLabel
                    control={<Checkbox checked={nightCare} onChange={handleChange('nightCare')} value="nightCare" />}
                    label="Night Care"
                  />
                </FormGroup>
              </FormControl>
            </Container>

            <Container style={{ margin: "30px 0 10px 0" }}>
              <FormControl component="fieldset" className={classes.formControl} style={{ width: "100%" }}>
                <FormLabel component="legend">Emergency rating</FormLabel>
                <FormGroup>
                  <FormControlLabel
                    control={<Checkbox checked={emergencyRatingLow} onChange={handleChange('emergencyRatingLow')} value="emergencyRatingLow" />}
                    label="Low"
                  />
                  <FormControlLabel
                    control={<Checkbox checked={emergencyRatingMedium} onChange={handleChange('emergencyRatingMedium')} value="emergencyRatingMedium" />}
                    label="Medium"
                  />
                  <FormControlLabel
                    control={<Checkbox checked={emergencyRatingHigh} onChange={handleChange('emergencyRatingHigh')} value="emergencyRatingHigh" />}
                    label="High"
                  />
                </FormGroup>
              </FormControl>
            </Container>


            <Container style={{ marginTop: "30px" }}>
              <FormControl component="fieldset" className={classes.formControl} style={{ width: "100%" }}>
                <FormLabel component="legend">Region</FormLabel>
                <FormGroup>
                  <FormControlLabel
                    control={<Checkbox checked={greenwich} onChange={handleChange('greenwich')} value="greenwich" />}
                    label="Greenwich"
                  />
                  <FormControlLabel
                    control={<Checkbox checked={kingston} onChange={handleChange('kingston')} value="kingston" />}
                    label="Kingston"
                  />
                </FormGroup>
              </FormControl>
            </Container>

            <div style={{ display: 'flex', justifyContent: "space-around" }}>
              <Button
                aria-controls="customized-menu"
                aria-haspopup="true"
                variant="contained"
                color="primary"
                style={{ fontSize: "12px", margin: "0 5px 0 5px" }}
              >
                Apply Now
              </Button>
              <Button
                aria-controls="customized-menu"
                aria-haspopup="true"
                variant="contained"
                onClick={resetFilters}
                style={{ fontSize: "12px", margin: "0 5px 0 5px" }}
              >
                Reset
              </Button>
            </div>
          </Paper>
        </div>
      </div>
    </DashboardLayout>
  );
}
