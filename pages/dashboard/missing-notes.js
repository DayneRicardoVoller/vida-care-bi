import React, { PureComponent } from 'react';
import {
  LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';
import DashboardLayout from '../../components/layouts/DashboardLayout';
import { TextField, Container, Button, Paper, Typography, FormControlLabel, Checkbox, Box, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, FormGroup, FormLabel, FormControl } from '@material-ui/core';
import { makeStyles} from '@material-ui/core/styles';

const data = [
  {
    name: '21 May', Unallocated: 0, pv: 0,
  },
  {
    name: '22 May', Unallocated: 0, pv: 0,
  },
  {
    name: '23 May', Unallocated: 0, pv: 0,
  },
  {
    name: '24 May', Unallocated: 0, pv: 0,
  },
  {
    name: '25 May', Unallocated: 0, pv: 0,
  },
  {
    name: '26 May', Unallocated: 0, pv: 0,
  },
  {
    name: '27 May', Unallocated: 2, pv: 2,
  },
];

const rows = [
  createData("09:00 - 10:00", 'Abraham Archer', 'Carer Required'),
  createData("09:00 - 10:00", 'Amelie Rowley', 'Aimee Freeman'),
  // createData("14:00 - 15:00", 'Amelie Gibson', 'Cameron Freeman'),
];

function createData(time, сlient, carer) {
  return { time, сlient, carer};
}

const useStyles = makeStyles(theme => ({
  table: {
    minWidth: 650,
  },
  container: {
    display: 'inline',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
    fontSize: "0.875rem",
    fontWeight: "500"
  },
  title: {
    margin: "30px"
  },
  button: {
    fontSize: "12px",
    margin: "0 5px 0 5px"
  },
  filtersContainer: {
    display: "flex",
    justifyContent: "center",
    margin: "30px 0 30px 0"
  },
  total: {
    padding: "8px",
    fontSize: "0.875rem",
    fontWeight: "500",
    border: "2px solid black"
  },
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      width: '250px',
    },
  },
}));

export default function MissingNotes() {

  const classes = useStyles();
  const [state, setState] = React.useState({
    liveInCare: true,
    dailyCare: false,
    nightCare: false,
    emergencyRatingLow: true,
    emergencyRatingMedium: false,
    emergencyRatingHigh: false,
    greenwich: true,
    kingston: false,
  });

  const handleChange = name => event => {
    setState({ ...state, [name]: event.target.checked });
  };

  const { liveInCare, dailyCare, nightCare, emergencyRatingLow, emergencyRatingMedium, emergencyRatingHigh, greenwich, kingston } = state;

  const resetFilters = () => {
    console.log("reset")
    setState({
      liveInCare: true,
      dailyCare: false,
      nightCare: false,
      emergencyRatingLow: true,
      emergencyRatingMedium: false,
      emergencyRatingHigh: false,
      greenwich: true,
      kingston: false,
    });
  }

  return (
    <DashboardLayout>
      <div style={{ display: "flex", height: 'calc(100% - 64px)' }}>
        <Container maxWidth='md' className={classes.container}>
          <Typography className={classes.title} align="center" component="h1" variant="h6" color="inherit" noWrap>Missing Notes</Typography>
          <LineChart
            width={730} height={250} data={data} style={{ margin: "0 auto" }}
            margin={{
              top: 5, right: 30, left: 20, bottom: 5
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Line  dataKey="Unallocated" stroke="#8884d8" activeDot={{ r: 8 }} />
          </LineChart>
          <div style={{
            display: "flex", marginTop: "20px", marginLeft: "150px", marginBottom: "20px"
          }}>
            <Box component="div" display="inline" style={{ width: "200px", marginBottom: "20px" }} >
              <Typography style={{ padding: "8px", fontSize: "0.875rem", fontWeight: "500", border: "2px solid black", display: "inline", marginBottom: "20px" }} >TOTAL: 2</Typography>
            </Box>
          </div>
          <Container style={{ display: 'flex', justifyContent: 'center', paddingLeft: "70px" }} className={classes.container}>
            <TableContainer style={{ width: '80%' }} component={Paper}>
              <Table  className={classes.table} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell align="left" >Time of visit</TableCell>
                    <TableCell align="left">Client</TableCell>
                    <TableCell align="left">Carer</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {rows.map(row => (
                    <TableRow key={row.сlient}>
                      <TableCell align="left">{row.time}</TableCell>
                      <TableCell align="left">{row.сlient}</TableCell>
                      <TableCell align="left">{row.carer}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Container>
        </Container>
        <div className={classes.root}>
          <Paper style={{ paddingTop: '30px' }}>
            <form className={classes.form} noValidate>
              <TextField
                id="fromDate"
                label="from"
                type="date"
                defaultValue="2020-05-21"
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
              />
              <TextField
                id="toDate"
                label="to"
                type="date"
                defaultValue="2020-05-28"
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </form>
            <Container style={{ margin: "30px 0 10px 0" }}>
              <FormControl component="fieldset" className={classes.formControl} style={{ width: "100%" }}>
                <FormLabel component="legend" >Care Type</FormLabel>
                <FormGroup>
                  <FormControlLabel
                    control={<Checkbox checked={liveInCare} onChange={handleChange('liveInCare')} value="liveInCare" />}
                    label="Live-in Care"
                  />
                  <FormControlLabel
                    control={<Checkbox checked={dailyCare} onChange={handleChange('dailyCare')} value="dailyCare" />}
                    label="Daily Care"
                  />
                  <FormControlLabel
                    control={<Checkbox checked={nightCare} onChange={handleChange('nightCare')} value="nightCare" />}
                    label="Night Care"
                  />
                </FormGroup>
              </FormControl>
            </Container>

            <Container style={{ margin: "30px 0 10px 0" }}>
              <FormControl component="fieldset" className={classes.formControl} style={{ width: "100%" }}>
                <FormLabel component="legend">Emergency rating</FormLabel>
                <FormGroup>
                  <FormControlLabel
                    control={<Checkbox checked={emergencyRatingLow} onChange={handleChange('emergencyRatingLow')} value="emergencyRatingLow" />}
                    label="Low"
                  />
                  <FormControlLabel
                    control={<Checkbox checked={emergencyRatingMedium} onChange={handleChange('emergencyRatingMedium')} value="emergencyRatingMedium" />}
                    label="Medium"
                  />
                  <FormControlLabel
                    control={<Checkbox checked={emergencyRatingHigh} onChange={handleChange('emergencyRatingHigh')} value="emergencyRatingHigh" />}
                    label="High"
                  />
                </FormGroup>
              </FormControl>
            </Container>


            <Container style={{ marginTop: "30px" }}>
              <FormControl component="fieldset" className={classes.formControl} style={{ width: "100%" }}>
                <FormLabel component="legend">Region</FormLabel>
                <FormGroup>
                  <FormControlLabel
                    control={<Checkbox checked={greenwich} onChange={handleChange('greenwich')} value="greenwich" />}
                    label="Greenwich"
                  />
                  <FormControlLabel
                    control={<Checkbox checked={kingston} onChange={handleChange('kingston')} value="kingston" />}
                    label="Kingston"
                  />
                </FormGroup>
              </FormControl>
            </Container>

            <div style={{ display: 'flex', justifyContent: "space-around" }}>
              <Button
                aria-controls="customized-menu"
                aria-haspopup="true"
                variant="contained"
                color="primary"
                style={{ fontSize: "12px", margin: "0 5px 0 5px" }}
              >
                Apply Now
                </Button>
              <Button
                aria-controls="customized-menu"
                aria-haspopup="true"
                variant="contained"
                onClick={resetFilters}
                style={{ fontSize: "12px", margin: "0 5px 0 5px" }}
              >
                Reset
                </Button>
            </div>
          </Paper>
        </div>
      </div>
    </DashboardLayout>
  );
}
