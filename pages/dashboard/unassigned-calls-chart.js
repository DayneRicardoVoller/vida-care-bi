import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  TextField,
  Container,
  Button,
  Typography,
  FormControlLabel,
  Checkbox,
  Box,
  Paper,
  FormGroup,
  FormLabel,
  FormControl,
} from "@material-ui/core";
import DashboardLayout from "../../components/layouts/DashboardLayout";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "inline",
    flexWrap: "wrap",
  },
  title: {
    margin: "30px",
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 240,
  },
  root: {
    display: "flex",
    flexWrap: "wrap",
    "& > *": {
      width: "250px",
    },
  },
  form: {
    display: "inline",
  },
  filtersContainer: {
    display: "flex",
    justifyContent: "center",
    margin: "30px 0 30px 0",
  },
  total: {
    padding: "8px",
    fontSize: "0.875rem",
    fontWeight: "500",
    border: "2px solid black",
  },
  button: {
    fontSize: "12px",
    margin: "0 5px 0 5px",
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
    fontSize: "0.875rem",
    fontWeight: "500",
  },
  dashboardContainer: {
    display: "flex",
    height: "calc(100% - 64px)",
  },
}));

const data = [
  {
    name: "19 May",
    "Number of Unassigned Calls": 10,
  },
  {
    name: "20 May",
    "Number of Unassigned Calls": 11,
  },
  {
    name: "21 May",
    "Number of Unassigned Calls": 13,
  },
  {
    name: "22 May",
    "Number of Unassigned Calls": 12,
  },
  {
    name: "23 May",
    "Number of Unassigned Calls": 7,
  },
  {
    name: "24 May",
    "Number of Unassigned Calls": 8,
  },
  {
    name: "25 May",
    "Number of Unassigned Calls": 12,
  },
  {
    name: "26 May",
    "Number of Unassigned Calls": 10,
  },
  {
    name: "27 May",
    "Number of Unassigned Calls": 9,
  },
];

function DashboardIndexPage() {
  const [state, setState] = React.useState({
    liveInCare: true,
    dailyCare: false,
    nightCare: false,
    emergencyRatingLow: true,
    emergencyRatingMedium: false,
    emergencyRatingHigh: false,
    greenwich: true,
    kingston: false,
    priorityLow: true,
    priorityMedium: false,
    priorityHigh: false,
  });

  const handleChange = (name) => (event) => {
    setState({ ...state, [name]: event.target.checked });
  };

  const {
    liveInCare,
    dailyCare,
    nightCare,
    emergencyRatingLow,
    emergencyRatingMedium,
    emergencyRatingHigh,
    greenwich,
    kingston,
    priorityLow,
    priorityMedium,
    priorityHigh,
  } = state;

  const resetFilters = () => {
    console.log("reset");
    setState({
      liveInCare: true,
      dailyCare: false,
      nightCare: false,
      emergencyRatingLow: true,
      emergencyRatingMedium: false,
      emergencyRatingHigh: false,
      greenwich: true,
      kingston: false,
      priorityLow: true,
      priorityMedium: false,
      priorityHigh: false,
    });
  };

  const classes = useStyles();
  return (
    <DashboardLayout>
      <div className={classes.dashboardContainer}>
        <Container maxWidth="md" className={classes.container}>
          <Typography
            className={classes.title}
            align="center"
            component="h1"
            variant="h6"
            color="inherit"
            noWrap
          >
            Unassigned Calls
          </Typography>
          <LineChart
            width={730}
            height={250}
            data={data}
            style={{ margin: "0 auto" }}
            margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Line dataKey="Number of Unassigned Calls" stroke="#82ca9d" />
          </LineChart>
          <div
            style={{
              display: "flex",
              marginTop: "20px",
              marginLeft: "150px",
            }}
          >
            <Box component="div" display="inline" style={{ width: "200px" }}>
              <Typography
                style={{
                  padding: "8px",
                  fontSize: "0.875rem",
                  fontWeight: "500",
                  border: "2px solid black",
                  display: "inline",
                }}
              >
                TOTAL: 92
              </Typography>
            </Box>
          </div>
        </Container>
        <div className={classes.root}>
          <Paper style={{ paddingTop: "30px" }}>
            <form className={classes.form} noValidate>
              <TextField
                id="fromDate"
                label="from"
                type="date"
                defaultValue="2020-05-21"
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
              />
              <TextField
                id="toDate"
                label="to"
                type="date"
                defaultValue="2020-05-29"
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </form>
            <Container style={{ margin: "30px 0 10px 0" }}>
              <FormControl
                component="fieldset"
                className={classes.formControl}
                style={{ width: "100%" }}
              >
                <FormLabel component="legend">Care Type</FormLabel>
                <FormGroup>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={liveInCare}
                        onChange={handleChange("liveInCare")}
                        value="liveInCare"
                      />
                    }
                    label="Live-in Care"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={dailyCare}
                        onChange={handleChange("dailyCare")}
                        value="dailyCare"
                      />
                    }
                    label="Daily Care"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={nightCare}
                        onChange={handleChange("nightCare")}
                        value="nightCare"
                      />
                    }
                    label="Night Care"
                  />
                </FormGroup>
              </FormControl>
            </Container>

            <Container style={{ margin: "30px 0 10px 0" }}>
              <FormControl
                component="fieldset"
                className={classes.formControl}
                style={{ width: "100%" }}
              >
                <FormLabel component="legend">Emergency Rating</FormLabel>
                <FormGroup>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={emergencyRatingLow}
                        onChange={handleChange("emergencyRatingLow")}
                        value="emergencyRatingLow"
                      />
                    }
                    label="Low"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={emergencyRatingMedium}
                        onChange={handleChange("emergencyRatingMedium")}
                        value="emergencyRatingMedium"
                      />
                    }
                    label="Medium"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={emergencyRatingHigh}
                        onChange={handleChange("emergencyRatingHigh")}
                        value="emergencyRatingHigh"
                      />
                    }
                    label="High"
                  />
                </FormGroup>
              </FormControl>
            </Container>

            <Container style={{ marginTop: "30px" }}>
              <FormControl
                component="fieldset"
                className={classes.formControl}
                style={{ width: "100%" }}
              >
                <FormLabel component="legend">Region</FormLabel>
                <FormGroup>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={greenwich}
                        onChange={handleChange("greenwich")}
                        value="greenwich"
                      />
                    }
                    label="Greenwich"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={kingston}
                        onChange={handleChange("kingston")}
                        value="kingston"
                      />
                    }
                    label="Kingston"
                  />
                </FormGroup>
              </FormControl>
            </Container>

            <Container style={{ margin: "30px 0 10px 0" }}>
              <FormControl
                component="fieldset"
                className={classes.formControl}
                style={{ width: "100%" }}
              >
                <FormLabel component="legend">Priority Rating</FormLabel>
                <FormGroup>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={priorityLow}
                        onChange={handleChange("priorityLow")}
                        value="priorityLow"
                      />
                    }
                    label="Low"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={priorityMedium}
                        onChange={handleChange("priorityMedium")}
                        value="priorityMedium"
                      />
                    }
                    label="Medium"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={priorityHigh}
                        onChange={handleChange("priorityHigh")}
                        value="priorityHigh"
                      />
                    }
                    label="High"
                  />
                </FormGroup>
              </FormControl>
            </Container>

            <div style={{ display: "flex", justifyContent: "space-around" }}>
              <Button
                aria-controls="customized-menu"
                aria-haspopup="true"
                variant="contained"
                color="primary"
                style={{ fontSize: "12px", margin: "0 5px 0 5px" }}
              >
                Apply Now
              </Button>
              <Button
                aria-controls="customized-menu"
                aria-haspopup="true"
                variant="contained"
                onClick={resetFilters}
                style={{ fontSize: "12px", margin: "0 5px 0 5px" }}
              >
                Reset
              </Button>
            </div>
          </Paper>
        </div>
         
      </div>
    </DashboardLayout>
  );
}

export default DashboardIndexPage;
