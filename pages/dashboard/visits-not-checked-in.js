import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Container,
  TextField,
  Typography,
  Box,
} from "@material-ui/core";
import DashboardLayout from "../../components/layouts/DashboardLayout";

const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 650,
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
  filtersContainer: {
    display: "flex",
    justifyContent: "center",
    margin: "30px 0 30px 0",
  },
  total: {
    padding: "8px",
    fontSize: "0.875rem",
    fontWeight: "500",
    border: "2px solid black",
  },
}));

function createData(date, time, сlient, carer, status) {
  return { date, time, сlient, carer, status };
}

const rows = [
  createData(
    "2020-05-28",
    "09:00 - 10:00",
    "Abraham Archer",
    "Carer Required",
    "Not checked in/out"
  ),
  createData(
    "2020-05-28",
    "09:00 - 10:00",
    "Amelie Rowley",
    "Aimee Freeman",
    "Not checked in/out"
  ),
  createData(
    "2020-05-28",
    "14:00 - 15:00",
    "Amelie Gibson",
    "Cameron Freeman",
    "Not checked in"
  ),
  createData(
    "2020-05-28",
    "09:00 - 13:00",
    "Annie Smith",
    "Abeed Mohamed",
    "Not checked in"
  ),
  createData(
    "2020-05-28",
    "14:00 - 15:00",
    "Ben Bruce",
    "Carer required",
    "Not checked in"
  ),
  createData(
    "2020-05-28",
    "10:30 - 11:00",
    "Connor Price",
    "Carer required",
    "Not checked in"
  ),
  createData(
    "2020-05-28",
    "10:40 - 11:10",
    "Isabel Anderson",
    "Carer required",
    "Not checked in"
  ),
  createData(
    "2020-05-28",
    "18:00 - 19:00",
    "Isabel Anderson",
    "Carer required",
    "Not checked in"
  ),
  createData(
    "2020-05-28",
    "11:00 - 12:00",
    "Jennifer Redfern",
    "Carer required",
    "Not checked in"
  ),
  createData(
    "2020-05-28",
    "09:35 - 10:20",
    "Keira Young",
    "Carer required",
    "Not checked in"
  ),
  createData(
    "2020-05-28",
    "12:00 - 14:00",
    "Leah Green",
    "Carer required",
    "Not checked in"
  ),
  createData(
    "2020-05-28",
    "17:55 - 16:10",
    "Morgan Savage",
    "Carer required",
    "Not checked in"
  ),
  createData(
    "2020-05-28",
    "15:40 - 16:40",
    "Leah Green",
    "Carer required",
    "Not checked in"
  ),
  createData(
    "2020-05-28",
    "19:20 - 19:50",
    "Leah Green",
    "Carer required",
    "Not checked in"
  ),
  createData(
    "2020-05-28",
    "09:00 - 10:00",
    "Kian Reid",
    "Carer required",
    "Not checked in"
  ),
  createData(
    "2020-05-28",
    "22:00 - 07:00",
    "Kian Reid",
    "Lydia Metcalfe",
    "Not checked in"
  ),
  createData(
    "2020-05-28",
    "19:50 - 20:50",
    "Jennifer Redfern",
    "Carer required",
    "Not checked in"
  ),
  createData(
    "2020-05-28",
    "18:20 - 19:20",
    "Isobel Black",
    "Carer required",
    "Not checked in"
  ),
  createData(
    "2020-05-28",
    "09:00 - 14:00",
    "Sarah Samson",
    "Carer required",
    "Not checked in"
  ),
  createData(
    "2020-05-28",
    "13:45 - 14:45",
    "Skye Rowe",
    "Carer required / Owen Middleton",
    "Not checked in"
  ),
  createData(
    "2020-05-28",
    "09:00 - 10:00",
    "Susie Mooley",
    "Carer required",
    "Not checked in"
  ),
  createData(
    "2020-05-28",
    "14:20 - 14:35",
    "Toby Archer",
    "Carer required",
    "Not checked in"
  ),
  createData(
    "2020-05-28",
    "09:00 - 10:00",
    "William Briggs",
    "Carer required",
    "Not checked in"
  )
];

export default function SimpleTable() {
  const classes = useStyles();

  return (
    <DashboardLayout>
      <Container maxWidth="md" className={classes.container}>
        <Typography
          align="center"
          component="h1"
          variant="h6"
          color="inherit"
          noWrap
        >
          Visits Not Checked In
        </Typography>
        <div className={classes.filtersContainer}>
          <Box component="div" display="inline">
            <form noValidate>
              <TextField
                id="date"
                label=""
                type="date"
                defaultValue="2020-05-28" //{appointment.date.start}
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </form>
          </Box>
          <Box component="div" display="inline">
            <form noValidate>
              <TextField
                id="time"
                label=""
                type="time"
                defaultValue="07:30"
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
                inputProps={{
                  step: 300, // 5 min
                }}
              />
            </form>
          </Box>
          <Box component="div" display="inline">
            <Typography className={classes.total}>
              TOTAL: {rows.length}
            </Typography>
          </Box>
        </div>
        <Container className={classes.container}>
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell align="left">Date of visit</TableCell>
                  <TableCell align="left">Time of visit</TableCell>
                  <TableCell align="left">Client</TableCell>
                  <TableCell align="left">Carer</TableCell>
                  <TableCell align="left">Status</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {rows.map((row) => (
                  <TableRow key={row.сlient}>
                    <TableCell align="left">{row.date}</TableCell>
                    <TableCell align="left">{row.time}</TableCell>
                    <TableCell align="left">{row.сlient}</TableCell>
                    <TableCell align="left">{row.carer}</TableCell>
                    <TableCell align="left">{row.status}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Container>
      </Container>
    </DashboardLayout>
  );
}